package com.example.hibernatecache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;
import java.io.File;

@SpringBootApplication
public class HibernatecacheApplication implements CommandLineRunner {

    public static void main(String[] args) {

        SpringApplication.run(HibernatecacheApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File("static/images/abc.txt");
        if (file.mkdir()) {
            System.out.println("File is created!");
        } else {
            System.out.println("File already exists.");
        }
    }
}
